import { MessageBody, OnGatewayConnection, SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';

@WebSocketGateway(8080, { namespace: 'tasks', cors: true })
export class TasksGateway implements OnGatewayConnection{
  @WebSocketServer()
  server: Server;

  private clients: Set<Socket> = new Set();

  handleConnection(client: Socket) {
    try {
      const token = client.handshake.query.token;
      client.data.user = token;
      this.clients.add(client);
      client.on('disconnect', () => this.clients.delete(client));
    } catch (err) {
      client.disconnect(); 
    }
  }

  @SubscribeMessage('newMessage')
  handleMessage(@MessageBody() body: any): void {
    this.clients.forEach(client => {
      if (client.data.user === 'JWTtest') {
        client.emit('onMessage', { text: 'Hello World'});
      }
    });
  }

  sendMessage(messageType: string, message: any): void {
    console.log(message);
    this.clients.forEach(client => {
      if (client.data.user === 'JWTtest') {
        client.emit(messageType, {message});
      }
    });
  }
}
