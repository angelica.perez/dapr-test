import { Injectable } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { TasksGateway } from './tasks.gateway';

@Injectable()
export class TasksService {

  constructor(private readonly gateway: TasksGateway) {}

  create(createTaskDto: CreateTaskDto) {
    console.log(createTaskDto);
    this.gateway.sendMessage('onMessage', createTaskDto);
  }

  findAll(payload: any) {
    console.log(payload);
    this.gateway.sendMessage('onMessage', payload);
  }
}
