import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { TasksService } from './tasks.service';
import { CreateTaskDto } from './dto/create-task.dto';

@Controller()
export class TasksController {
  constructor(private readonly tasksService: TasksService) {}

  @MessagePattern('task_created_notify')
  create(@Payload() createTaskDto: CreateTaskDto) {
    this.tasksService.create(createTaskDto);
  }

  @MessagePattern('tasks_list_notify')
  findAll(@Payload() payload: any) {
    this.tasksService.findAll(payload);
  }
}
