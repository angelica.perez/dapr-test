import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';

const KAFKA_BROKER = process.env.KAFKA_BROKER || 'localhost:9094';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      transport: Transport.KAFKA,
      options: {
        client: {
          brokers: [KAFKA_BROKER],
        },
        consumer: {
          groupId: 'update-vector-consumer'
        }
      }
    }
  );
  await app.listen();

}
bootstrap();