import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { ClientKafka, MessagePattern, Payload } from '@nestjs/microservices';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @MessagePattern('update_vector')
  updateVector(@Payload() data: any) {
    console.log('updateVector triggered', data);
    return this.appService.updateVector(data.tenantId, data.backupPrefix);
  }
}
