import { Injectable } from '@nestjs/common';
import {OpenAIClient, AzureKeyCredential} from '@azure/openai';
const { MilvusClient, DataType } = require("@zilliz/milvus2-sdk-node");

@Injectable()
export class MilvusService {
  private endpoint: string;
  private apiKey: string;
    constructor() {
      this.endpoint = process.env.OPENAI_ENDPOINT || 'https://test-timp-arch.openai.azure.com';
      this.apiKey = process.env.OPENAI_API_KEY || '5216ad635a14445a873b6afa4aa4cd16';
    }
    async replicateToMilvus(tableName: string, element: any) {
        // Convert element to vector - this part is up to you to implement based on your data
        await this.convertTaskToVector(element);
        const vector = await this.convertTaskToVector(element);

        // Insert the vector into Milvus
        await this.insertIntoMilvus(tableName, vector, element._id);
    }
    
    
    async convertTaskToVector(element) {
        const client = new OpenAIClient(this.endpoint, new AzureKeyCredential(this.apiKey));
        const deploymentId = "embedd-ada-test";
          try {

            const data = JSON.stringify(element);
            // const data = Object.values(element).join(" ");

            const embeddings = await client.getEmbeddings(deploymentId, [data]);
        
            // Assuming the API returns an array of embeddings for each input
            // Adjust according to the actual API response structure
            const vector = embeddings.data[0].embedding;
            return vector;
          } catch (error) {
            console.error("Error generating embedding:", error);
            return null;
          }
    }
    
    async insertIntoMilvus(tableName: string, vector, id) {
        const milvusClient = new MilvusClient("localhost:19530");
        try {
            // define schema
            const collection_name = 'Task3';
            // const schema = [
            //     {
            //       name: 'id',
            //       description: 'ID field',
            //       data_type: DataType.VarChar,
            //       is_primary_key: true,
            //       auto_id: false,
            //       max_length: 128
            //     },
            //     {
            //       name: 'vector',
            //       description: 'Vector field',
            //       data_type: DataType.FloatVector,
            //       dim: 1536,
            //     }
            //   ];

              // const response = await milvusClient.createCollection({
              //   collection_name,
              //   fields: schema,
              // });
              
              const response = await milvusClient.insert({
                  collection_name,
                  fields_data: [{
                      // Assuming "vector" is the field name in your Milvus collection schema for embeddings
                      id,
                      vector: vector,
                    }]
                  });
        
            // return response;
          } catch (error) {
            console.error("Error inserting into Milvus:", error);
            return null;
          }
    }
}
