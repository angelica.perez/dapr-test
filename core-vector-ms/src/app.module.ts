import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MinioService } from './minio/minio.service';
import { MilvusService } from './milvus/milvus.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, MinioService, MilvusService],
})
export class AppModule {}
