import { Injectable } from '@nestjs/common';
import { MinioService } from './minio/minio.service';
import { Neo4jService } from './neo4j/neo4j.service';

@Injectable()
export class AppService {
  constructor(private readonly minioService: MinioService, private readonly neo4jService: Neo4jService) {}
  async updateGraph(tenantId: string, backupPrefix: string){
    console.log('Update vector event received');
    const tableList = await this.minioService.listObjects(tenantId, backupPrefix);
    console.log(tableList);
    //@ts-ignore
    const tableNames = tableList.map(table => table.name.split('/')[1].split('.')[0]);
    console.log(tableNames);
    const jsonFiles = await this.minioService.readLatestFolder(tenantId, backupPrefix);
    console.log(jsonFiles);
    
    tableNames.forEach(async (tableName, index) => {
      const data = JSON.parse(jsonFiles[index]);
      data.forEach(async(item, i) => {
        item.tenantId = tenantId;
        console.log(tenantId, tableName, item);
        await this.neo4jService.replicateDataToNeo4j(item);
      })
    });
  }
}
