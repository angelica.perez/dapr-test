// src/services/neo4j.service.ts
import { Injectable } from '@nestjs/common';
import neo4j from 'neo4j-driver';

@Injectable()
export class Neo4jService {
  private driver;

  constructor() {
    const neo4jHost = process.env.NEO4J_HOST || 'localhost';
    const neo4jPort = process.env.NEO4J_PORT || '7687';
    const neo4jUser = process.env.NEO4J_USER || 'neo4j';
    const neo4jPassword = process.env.NEO4J_PASSWORD || '123456789';
    this.driver = neo4j.driver(`neo4j://${neo4jHost}:${neo4jPort}`, neo4j.auth.basic(neo4jUser, neo4jPassword));
  }

  async replicateDataToNeo4j(dataObject) {
      const session = this.driver.session();
      // Generate a dynamic Cypher query based on JSON keys
      const { query, params } = this.buildMergeQuery(dataObject);
      await session.run(query, params);
      await session.close();
  }

  buildMergeQuery(dataObject: any) {
    const keys = Object.keys(dataObject);
    const id = dataObject._id; // Assuming each object has a unique 'id' field
    if (!id) throw new Error('Data object missing unique identifier: ' + JSON.stringify(dataObject));

    let setStatements = keys.map(key => `n.${key} = $${key}`).join(', ');
    let params = keys.reduce((acc, key) => ({ ...acc, [key]: dataObject[key] }), {});

    console.log('params', params)

    const query = `
      MERGE (n { id: $id })
      ON CREATE SET ${setStatements}
      ON MATCH SET ${setStatements}
    `;

    console.log('query', query)

    return { query, params: { ...params, id } };
  }
}
