import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MinioService } from './minio/minio.service';
import { Neo4jService } from './neo4j/neo4j.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, MinioService, Neo4jService],
})
export class AppModule {}
