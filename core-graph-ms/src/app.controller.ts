import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { ClientKafka, MessagePattern, Payload } from '@nestjs/microservices';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @MessagePattern('update_vector')
  updateGraph(@Payload() data: any) {
    console.log('updateVector triggered', data);
    return this.appService.updateGraph(data.tenantId, data.backupPrefix);
  }
}