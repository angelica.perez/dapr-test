import { Injectable, NestMiddleware, BadRequestException } from '@nestjs/common';
import { Request, Response, NextFunction} from 'express';

@Injectable()
export class TenantMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    const tenantId = req.header('tenant-id')?.toString(); // Get the tenant id from the request header
    if (!tenantId) {
      throw new BadRequestException('Tenant id is required');
    }
    console.log('Tenant id:', tenantId);
    req['tenantId'] = tenantId; // Add the tenant id to the request object
    next();
  }
}
