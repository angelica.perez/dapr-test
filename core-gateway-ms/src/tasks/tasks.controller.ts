import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Req,
} from '@nestjs/common';
import { TasksService } from './tasks.service';
import { CreateTaskDto } from './dto/create-task.dto';
import { ApiTags, ApiResponse, ApiBody } from '@nestjs/swagger';

@ApiTags('Tasks')
@Controller('tasks')
export class TasksController {
  constructor(private readonly tasksService: TasksService) {}

  @Post()
  @ApiResponse({
    status: 201,
    description: 'The record has been successfully created.',
  })
  @ApiResponse({
    status: 400,
    description: 'There are some error in the JSON data.',
  })
  @ApiBody({
    type: CreateTaskDto,
    description: 'Json structure for task object',
  })
  create(@Body() createTaskDto: CreateTaskDto, @Req() { tenantId }) {
    return this.tasksService.create(createTaskDto, tenantId);
  }
  @Get('vector')
  findVectorTask(@Body() data: any, @Req() { tenantId }) {
    console.log('data', data);
    return this.tasksService.findVectorTask(data, tenantId);
  }

  @Get('graph')
  findGraphTask(@Body() data: any, @Req() { tenantId }) {
    console.log('data', data);
    return this.tasksService.findGraphTask(data, tenantId);
  }

  @Get()
  findAll(@Req() { tenantId }) {
    return this.tasksService.findAll(tenantId);
  }

  @Get('test')
  findOne() {
    return this.tasksService.findOne();
  }

  @Patch(':id')
  update(@Param('id') id: string) {
    return this.tasksService.update(+id);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.tasksService.remove(+id);
  }
}
