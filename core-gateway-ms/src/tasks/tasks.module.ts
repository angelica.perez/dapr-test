import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { TasksController } from './tasks.controller';
import { TenantMiddleware } from '../tenant/tenant.middleware';
import { DaprClient } from 'dapr-client';

@Module({
  imports: [],
  controllers: [TasksController],
  providers: [
    TasksService,
    {
      provide: DaprClient,
      useValue: new DaprClient(),
    },
  ],
})
export class TasksModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(TenantMiddleware).forRoutes(TasksController);
  }
}
