import { Injectable } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
// import { UpdateTaskDto } from './dto/update-task.dto';
// import { ClientKafka } from '@nestjs/microservices';
import { CreateTaskEvent } from './events/createTask.event';
import { DaprClient } from 'dapr-client';

@Injectable()
export class TasksService {
  constructor(
    // @Inject('TASKS_SERVICE') private readonly taskClient: ClientKafka,
    private readonly daprClient: DaprClient,
  ) {}

  create(createTaskDto: CreateTaskDto, tenantId) {
    // this.taskClient.emit(
    //   'create_task_event',
    //   new CreateTaskEvent(
    //     createTaskDto.name,
    //     createTaskDto.description,
    //     createTaskDto.owner,
    //     createTaskDto.weekday,
    //     tenantId,
    //   ),
    // );
    const response = this.daprClient.pubsub.publish(
      'pubsub',
      'create_task_event',
      new CreateTaskEvent(
        createTaskDto.name,
        createTaskDto.description,
        createTaskDto.owner,
        createTaskDto.weekday,
        tenantId,
      ),
    );
    console.log(response);
    return 'A new task will be created 3';
  }

  findVectorTask(data: any, tenantId) {
    this.daprClient.pubsub.publish('pubsub', 'find_vector_event', {
      text: data.text,
      collection: data.collection,
      tenantId,
    });
    return 'This action will find a vector closest to the task description';
  }

  findGraphTask(data: any, tenantId) {
    this.daprClient.pubsub.publish('pubsub', 'find_graph_event', {
      data: data.text,
      tenantId,
    });
    return 'This action will connect to chatgpt and generate a reponse based on the text';
  }

  findAll(tenantId: string) {
    this.daprClient.pubsub.publish('pubsub', 'list_tasks_event', { tenantId });
    return `This action returns all tasks`;
  }

  findOne() {
    this.daprClient.pubsub.publish('pubsub', 'read_view_event', {});
    return `This action will read a SAP HANA View`;
  }

  update(id: number) {
    return `This action updates a #${id} task`;
  }

  remove(id: number) {
    return `This action removes a #${id} task`;
  }
}
