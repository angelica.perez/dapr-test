export class CreateTaskEvent {
    constructor(
      public readonly name: string,
      public readonly description: string,
      public readonly owner: string,
      public readonly weekday: string,
      public readonly tenantId: string
    ) {}
  
    toString() {
      return JSON.stringify({
        name: this.name,
        description: this.description,
        owner: this.owner,
        weekday: this.weekday,
        tenantId: this.tenantId
      });
    }
  }