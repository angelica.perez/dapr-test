import { IsNotEmpty, IsString, MaxLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateTaskDto {
    @ApiProperty({
        example: 'Task 1',
        required: true,
     })
    @MaxLength(64)
    @IsString()
    @IsNotEmpty()
    name: string;

    @ApiProperty({
        example: 'Description for the Task 1',
        required: true,
     })
    @MaxLength(255)
    @IsString()
    @IsNotEmpty()
    description: string;

    @ApiProperty({
        example: 'User 1',
        required: true,
     })
    @MaxLength(64)
    @IsString()
    @IsNotEmpty()
    owner: string;

    @ApiProperty({
        example: 'Monday',
        required: true,
     })
    @MaxLength(64)
    @IsString()
    @IsNotEmpty()
    weekday: string;

}
