import { Injectable } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { HanaClientDb } from 'src/hana-client-db/hana-client-db';
import { Task } from './entities/task.entity';
import { PostgresService } from '../postgres/postgres.service';
import { DataSource } from 'typeorm';
import { MilvusService } from '../milvus/milvus.service';
import { OpenaiService } from '../openai/openai.service';
import { Neo4jService } from 'src/neo4j/neo4j.service';
import { DaprClient } from 'dapr-client';

const MONGO_HOST = process.env.MONGO_HOST || 'localhost';
const MONGO_PORT = Number(process.env.MONGO_PORT) || 27017;
const MONGO_USER = process.env.MONGO_USER || '';
const MONGO_PASSWORD = process.env.MONGO_PASSWORD || '';

@Injectable()
export class TasksService {
  private hanaClient: HanaClientDb;

  constructor(
    private readonly daprClient: DaprClient,
    private readonly postgresService: PostgresService,
    private readonly milvusService: MilvusService,
    private readonly openaiService: OpenaiService,
    private readonly neo4jService: Neo4jService,
  ) {
    this.hanaClient = new HanaClientDb();
  }
  async create(createTaskDto: CreateTaskDto) {
    console.log(createTaskDto);
    const connection = new DataSource({
      type: 'mongodb',
      host: MONGO_HOST,
      port: MONGO_PORT,
      username: MONGO_USER,
      password: MONGO_PASSWORD,
      database: 'timp',
      entities: [Task],
    });

    const task = {
      name: createTaskDto.name,
      description: createTaskDto.description,
      owner: createTaskDto.owner,
      weekday: createTaskDto.weekday,
    };

    connection
      .initialize()
      .then(() => {
        console.log('Connection to MongoDB has been established successfully');
        connection
          .getMongoRepository(Task)
          .save(task)
          .then((resp) => {
            this.daprClient.pubsub.publish('pubsub', 'task_created_notify', {
              status: 'success',
            });
            this.daprClient.pubsub.publish('pubsub', 'update_storage', {
              tenantId: createTaskDto.tenantId,
            });
            console.log('The task has been inserted successfully', resp);
          });
      })
      .catch((error) => {
        console.error('Error connecting to MongoDB:', error);
      });
  }

  async findAll(payload) {
    console.log('finAll triggered');
    const query = 'SELECT * FROM "public"."task"';
    try {
      const data = await this.postgresService.query(query, payload.tenantId);
      this.daprClient.pubsub.publish('pubsub', 'tasks_list_notify', data);
    } catch (error) {
      console.error('Error executing query:', error);
    }
    // this.hanaClient.executeSelect('SELECT * FROM APEREZ.TASKS')
    // .then((resp) => {
    //   this.notifyClient.emit('tasks_list_notify', resp);
    //   console.log(resp);
    // });
  }

  readView() {
    console.log('readView triggered');
    this.hanaClient
      .executeSelect(
        'SELECT TOP 3 "MANDT", "COD_EMP", "NOME_EMPRESA" FROM "_SYS_BIC"."timp.atr.modeling.emp_fil/EMPRESA_FILIAL_CADASTRO" (' +
          "'PLACEHOLDER' = ('$$IP_MANDANTE$$', '300'))",
      )
      .then((resp) => {
        this.daprClient.pubsub.publish('pubsub', 'tasks_list_notify', resp);
        console.log(resp);
      });
  }

  async findVector(data) {
    console.log(data);
    console.log('findVector triggered');
    const queryEmbedding = await this.openaiService.generateQueryEmbedding(
      data.text,
    );
    const searchResultIds = await this.milvusService.searchInMilvus(
      data.collection,
      queryEmbedding,
    );
    console.log(searchResultIds);
    let ids = '';
    searchResultIds.forEach((element, index) => {
      ids += `'${element}'`;
      if (index != searchResultIds.length - 1) {
        ids += ',';
      }
    });
    console.log(ids);
    const query = `SELECT * FROM "public"."task" WHERE _id IN (${ids})`;
    try {
      const result = await this.postgresService.query(query, data.tenantId);
      console.log(result);
      this.daprClient.pubsub.publish('pubsub', 'tasks_list_notify', result[0]);
    } catch (error) {
      console.error('Error executing query:', error);
    }
    // const documents = await this.postgresService.fetchElements(searchResults);
  }

  async findGraph(data) {
    console.log(data);
    console.log('findGraph triggered');
    const keywords = await this.openaiService.extractKeywords(data.data);
    const tasksDetails = await this.neo4jService.findTaskDetails(keywords);
    console.log(tasksDetails);
    this.daprClient.pubsub.publish('pubsub', 'tasks_list_notify', tasksDetails);
  }
}
