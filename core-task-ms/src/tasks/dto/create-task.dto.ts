export class CreateTaskDto {
    name: string;
    description: string;
    owner: string;
    weekday: string;
    tenantId: string;
}
