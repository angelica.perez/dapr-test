import { TasksService } from './tasks.service';
import { TasksController } from './tasks.controller';
import { PostgresService } from '../postgres/postgres.service';
import { Module } from '@nestjs/common';
import { MilvusService } from 'src/milvus/milvus.service';
import { OpenaiService } from 'src/openai/openai.service';
import { Neo4jService } from 'src/neo4j/neo4j.service';
import { DaprClient } from 'dapr-client';

@Module({
  imports: [],
  controllers: [TasksController],
  providers: [
    TasksService,
    PostgresService,
    MilvusService,
    OpenaiService,
    Neo4jService,
    DaprClient,
  ],
})
export class TasksModule {}
