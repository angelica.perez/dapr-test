import { Column, Entity, ObjectId, ObjectIdColumn } from 'typeorm';

@Entity('petrobras__core_task_ms__tasks')
export class Task {

  @ObjectIdColumn()
  id: ObjectId;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  owner: string;

  @Column()
  weekday: string;

}