import { Controller } from '@nestjs/common';
// import { MessagePattern, Payload } from '@nestjs/microservices';
import { TasksService } from './tasks.service';
import { DaprPubSubSubscribe } from 'src/decorators/pubSub.decorator';
import { CreateTaskDto } from './dto/create-task.dto';

@Controller()
export class TasksController {
  constructor(private readonly tasksService: TasksService) {}

  @DaprPubSubSubscribe('pubsub', 'create_task_event')
  // @MessagePattern('create_task_event')
  create(createTaskDto: CreateTaskDto) {
    console.log(createTaskDto);
    this.tasksService.create(createTaskDto);
  }

  // @DaprPubSubSubscribe('pubsub', 'find_vector_event')
  // // @MessagePattern('find_vector_event')
  // findVector(data: any) {
  //   this.tasksService.findVector(data);
  // }

  // @DaprPubSubSubscribe('pubsub', 'find_graph_event')
  // // @MessagePattern('find_graph_event')
  // findGraph(data: any) {
  //   this.tasksService.findGraph(data);
  // }

  // @MessagePattern('list_tasks_event')
  @DaprPubSubSubscribe('pubsub', 'list_tasks_event')
  findAll(payload: any) {
    console.log(payload);
    this.tasksService.findAll(payload);
  }

  // @DaprPubSubSubscribe('pubsub', 'read_view_event')
  // // @MessagePattern('read_view_event')
  // readView() {
  //   this.tasksService.readView();
  // }
}
