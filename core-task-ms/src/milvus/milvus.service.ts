import { Injectable } from '@nestjs/common';
import { MilvusClient } from "@zilliz/milvus2-sdk-node";

@Injectable()
export class MilvusService {
    private milvusClient: any;
    constructor() {
        this.milvusClient = new MilvusClient("localhost:19530");
    }
    async searchInMilvus(collectionName, queryEmbedding) {
        await this.milvusClient.createIndex({
            // required
            collection_name: 'Task3',
            field_name: 'vector', // optional if you are using milvus v2.2.9+
            index_name: 'myindex', // optional
            index_type: 'HNSW', // optional if you are using milvus v2.2.9+
            params: { efConstruction: 10, M: 4 }, // optional if you are using milvus v2.2.9+
            metric_type: 'L2', // optional if you are using milvus v2.2.9+
          });
        await this.milvusClient.loadCollectionSync({
            collection_name: 'Task3',
          });
        const searchParams = {
          collection_name: 'Task3',
          vectors: queryEmbedding,
          params: { nprobe: 64 }, // optional, specify the search parameters
          metric_type: 'L2', // optional, metric to calculate similarity of two vectors
        };
      
        const {results} = await this.milvusClient.search(searchParams);
        console.log(results)
        const ids = results.map(res => res.id);
        return ids;
    }
}
