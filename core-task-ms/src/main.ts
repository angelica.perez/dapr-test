import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DaprServer } from 'dapr-client';
import { useDaprPubSubListener } from './decorators/pubSub.decorator';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const server = app.get(DaprServer);
  await useDaprPubSubListener(app);
  await server.start();
}
bootstrap();
