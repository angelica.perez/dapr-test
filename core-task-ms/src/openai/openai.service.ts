import { Injectable } from '@nestjs/common';
import {OpenAIClient, AzureKeyCredential} from '@azure/openai';
import axios from 'axios';

@Injectable()
export class OpenaiService {
  private endpoint: string;
  private apiKey: string;
  constructor() {
    this.endpoint = process.env.OPENAI_ENDPOINT || 'https://test-timp-arch.openai.azure.com';
    this.apiKey = process.env.OPENAI_API_KEY || '5216ad635a14445a873b6afa4aa4cd16';
  }

  async extractKeywords(question: string): Promise<string[]> {
    try {
        const client = new OpenAIClient(this.endpoint, new AzureKeyCredential(this.apiKey));
        const deploymentId = "gpt35-test-graph";
        const result = await client.getCompletions(deploymentId, [`
          Extract 3 keywords from the corresponding texts below.

          Text 1: Stripe provides APIs that web developers can use.
          Keywords 1: Stripe, APIs, developers
        
          Text 2: OpenAI has trained cutting-edge language models that are very good at understanding and generating text. 
          Keywords 2: OpenAI, models, text
        
          Text 3: ${question}
          Keywords 3:

        `], { maxTokens: 30, temperature: 0 });
        // console.log(result.choices[0].text.trim().split('-').join('').split('\n').join('').split(','));

        // Process the response to extract keywords
        // The structure of the response and how you extract keywords will depend on how Azure's API returns the data
        const text = result.choices[0].text;

        const keywords = text.trim().split('-').join('').split(':')[1].split('\n').join('').split('2')[0].split(' ').join('').split(',');
        // console.log(keywords);

        return keywords;
    } catch (error) {
        console.error('Failed to extract keywords with Azure OpenAI Service:', error);
        return []; // Return an empty array or handle the error as appropriate
    }
  }

  async generateResponse(taskDetails: any[]): Promise<string[]> {
    try {
      let jsonData = []
      taskDetails.forEach(arr => {
        arr.forEach(record => {
          let obj = {};
          for(let i = 0; i < record.keys.length; i++){
            obj[record.keys[i]] = record._fields[i];
          } 
          jsonData.push(obj);
        })
      });

        const client = new OpenAIClient(this.endpoint, new AzureKeyCredential(this.apiKey));
        const deploymentId = "gpt35-test-graph";
        const result = await client.getCompletions(deploymentId, [`
          The array bellow contains Javascript's objects with information about tasks

          Array: ${jsonData}

          Extract the tasks of the array.

          First extract the name of the task, then extract the description of the task, then the weekday and finally the owner.
          
          Desired output format:
          TASKS:
          * name: description, on weekday by owner
          * name: description, on weekday by owner
          * name: description, on weekday by owner
          
          Example:
          Array: [{"name": "Walk", "description": "to the park", "owner": "john", "weekday": "Monday"}]

          Output:

          TASKS:
          * Walk: to the park, on Monday by John

        `], { maxTokens: 150, temperature: 0});
        console.log(result.choices[0].text);

        // Process the response to extract keywords
        // The structure of the response and how you extract keywords will depend on how Azure's API returns the data
        // const text = result.choices[0].text;

        // const keywords = text.trim().split('-').join('').split(':')[1].split('\n').join('').split('2')[0].split(' ').join('').split(',');
        // console.log(keywords);

        // return keywords;
    } catch (error) {
        console.error('Failed to extract keywords with Azure OpenAI Service:', error);
        return []; // Return an empty array or handle the error as appropriate
    }
  }

    async generateQueryEmbedding(data) {
      const client = new OpenAIClient(this.endpoint, new AzureKeyCredential(this.apiKey));
      const deploymentId = "embedd-ada-test";
        try {

          const embeddings = await client.getEmbeddings(deploymentId, [data]);

          const vector = embeddings.data[0].embedding;
          return vector;
        } catch (error) {
          console.error("Error generating embedding:", error);
          return null;
        }
      }
}
