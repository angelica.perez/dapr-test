import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { TasksModule } from './tasks/tasks.module';
import { PostgresService } from './postgres/postgres.service';
import { MilvusService } from './milvus/milvus.service';
import { OpenaiService } from './openai/openai.service';
import { Neo4jService } from './neo4j/neo4j.service';
import { DaprServer } from 'dapr-client';

@Module({
  imports: [EventEmitterModule.forRoot(), TasksModule],
  controllers: [AppController],
  providers: [
    AppService,
    PostgresService,
    MilvusService,
    OpenaiService,
    Neo4jService,
    DaprServer,
  ],
})
export class AppModule {}
