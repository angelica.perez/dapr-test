import { Injectable, OnModuleInit, OnModuleDestroy } from '@nestjs/common';
import { Pool, Client } from 'pg';

@Injectable()
export class PostgresService {
    private pool: Pool;

  constructor() {
    // this.pool = new Pool({
    //     user: process.env.POSTGRES_USER || 'citizix_user',
    //     host: process.env.POSTGRES_HOST || 'localhost',
    //     database: process.env.POSTGRES_DB || 'citizix_db',
    //     password: process.env.POSTGRES_PASSWORD || 'S3cret',
    //     port: 5432,
    // });
  }

  // async onModuleInit() {
  //   await this.pool.connect();
  // }

  // async onModuleDestroy() {
  //   await this.pool.end();
  // }

  async query(text: string, tenantId: string, params?: any[]) {
    this.pool = new Pool({
      user: process.env.POSTGRESQL_USER || 'citizix_user',
      host: process.env.POSTGRESQL_HOST || 'localhost',
      database: tenantId,
      password: process.env.POSTGRESQL_PASSWORD || 'S3cret',
      port: Number(process.env.POSTGRESQL_PORT) || 5432,
    });
    const client = await this.pool.connect();
    try {
      const result = await client.query(text, params);
      return result.rows;
    } finally {
      client.release();
    }
  }
}
