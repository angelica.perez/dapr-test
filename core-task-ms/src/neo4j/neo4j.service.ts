import { Injectable } from '@nestjs/common';
import neo4j from 'neo4j-driver';

@Injectable()
export class Neo4jService {
    private driver;

  constructor() {
    const neo4jHost = process.env.NEO4J_HOST || 'localhost';
    const neo4jPort = process.env.NEO4J_PORT || '7687';
    const neo4jUser = process.env.NEO4J_USER || 'neo4j';
    const neo4jPassword = process.env.NEO4J_PASSWORD || '123456789';
    this.driver = neo4j.driver(`neo4j://${neo4jHost}:${neo4jPort}`, neo4j.auth.basic(neo4jUser, neo4jPassword));
  }

  async findTaskDetails(keywords: string[]) {
    try {
        // Construct a Cypher query to find tasks based on keywords
        // This example assumes tasks are stored with 'name', 'description', 'owner', and 'weekday' properties
        const query = `
          MATCH (t)
          WHERE t.name CONTAINS $keyword OR t.description CONTAINS $keyword OR t.owner CONTAINS $keyword OR t.weekday CONTAINS $keyword
          RETURN t.name AS name, t.description AS description, t.owner AS owner, t.weekday AS weekday
        `;

        let data = [];
  
        // Execute the query for each keyword until a match is found
        for (const keyword of keywords) {
            const session = this.driver.session();
            const result = await session.run(query, { keyword });
            // console.log(result.records);
            if (result.records.length > 0) {
                // Assuming we take the first match for simplicity
                data.push(result.records);
            }
            await session.close();
        }

        let jsonData = []
        data.forEach(arr => {
            arr.forEach(record => {
            let obj = {};
            for(let i = 0; i < record.keys.length; i++){
                obj[record.keys[i]] = record._fields[i];
            } 
            jsonData.push(obj);
            })
        });

        let output = `TASKS: \n`;
        jsonData.forEach(el => {
            let line = ` * ${el.name}: ${el.description}, on ${el.weekday} by ${el.owner} \n`;
            output += line;
        });

        return output;
  
        // return "Sorry, I couldn't find any relevant tasks based on your question.";
      } catch(e) {
        console.log(e)
      }
}
}
