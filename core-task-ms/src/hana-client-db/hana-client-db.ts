let hana = require('@sap/hana-client');
var PromiseModule = require('@sap/hana-client/extension/Promise.js');
export class HanaClientDb {
    private conn: any;
    private connectionParams: any;

  constructor() {
    this.conn = hana.createConnection();
    this.connectionParams = {
        host: 'as1-100-01',
        port: 30044,
        uid: 'APEREZ',
        pwd: 'Mar-6711'
      };
  }

  executeSelect (query: string) {
    return PromiseModule.connect(this.conn, this.connectionParams)
        .then(() => {
            return PromiseModule.prepare(this.conn, query);
        })
        .then((stmt) => {
            return PromiseModule.exec(stmt);
        })
        .catch(err => {
            console.log(err);
        })
        .finally(() => {
            PromiseModule.close(this.conn);
            console.log('Connection closed');
        });
  }

  executeInsert (query: string, values: any[]) {
    return PromiseModule.connect(this.conn, this.connectionParams)
        .then(() => {
            return PromiseModule.prepare(this.conn, query);
        })
        .then((stmt) => {
            return PromiseModule.exec(stmt, values);
        })
        .catch(err => {
            console.log(err);
        })
        .finally(() => {
            PromiseModule.close(this.conn);
            console.log('Connection closed');
        });
  }
}
