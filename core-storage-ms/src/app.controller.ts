import { Controller, Inject } from '@nestjs/common';
import { AppService } from './app.service';
import { ClientKafka, MessagePattern, Payload } from '@nestjs/microservices';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService, 
    @Inject('UPDATE_SQL_SERVICE') private readonly updateSqlClient: ClientKafka,
    @Inject('UPDATE_VECTOR_SERVICE') private readonly updateVectorClient: ClientKafka) {}

  @MessagePattern('update_storage')
  updateBackup(@Payload() data: any) {
    this.appService.checkIfBucketExists(data.tenantId).then((exists) => {
      console.log(exists)
      if (exists) {
        this.appService.updateBackup('test-timp').then((res) => {
          this.updateSqlClient.emit('update_sql', {tenantId: data.tenantId, backupPrefix: res});
          // this.updateVectorClient.emit('update_vector', {tenantId: data.tenantId, backupPrefix: res});
          // this.updateVectorClient.emit('update_graph', {tenantId: data.tenantId, backupPrefix: res});
        });
      } 
    });
  }
}
