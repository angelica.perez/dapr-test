import { Inject, Injectable } from '@nestjs/common';
import { MongoClient } from 'mongodb';
const { Client } = require("minio");

@Injectable()
export class AppService {
  private minioClient: any;
  constructor(@Inject('DATABASE_CONNECTION') private readonly mongoClient: MongoClient) {
    this.minioClient = new Client({
      endPoint: process.env.S3_HOST || 'localhost',
      port: Number(process.env.S3_PORT) || 9000,
      useSSL: false,
      accessKey: process.env.S3_USER || 'minioadmin',
      secretKey: process.env.S3_PASSWORD || 'minioadmin'
    });
  }

  checkIfBucketExists = (bucketName: string) =>  {
    return new Promise((resolve, reject) => {
      this.minioClient.bucketExists(bucketName, function(err, exists) {
        if (err) {
          reject(err);
        } else {
          resolve(exists);
        }
      });
    })
  }

  createBucket = (bucketName: string) => {
    return new Promise((resolve, reject) => {
      this.minioClient.makeBucket(bucketName, function(err) {
        if (err) {
          reject(err);
        } else {
          resolve(`Bucket: ${bucketName} created successfully in "us-east-1"`);
        }
      });
    });
  }

  async updateBackup(bucketName: string) {
    console.log('updateBackup triggered');

    const collectionsArray = await this.mongoClient.db('timp').listCollections().toArray();
 
    // storing the names of the collections separately
    var collectionNames = [];

    collectionsArray.forEach((item) => {
      collectionNames.push(item.name);
    });

    // console.log(collectionsArray)

    const uploadFile = (bucketName, objectName, file) => {
      return new Promise((resolve, reject) => {
        this.minioClient.putObject(bucketName, objectName, file, function(err, etag) {
          if (err) {
            reject(err);
          } else {
            resolve(etag);
          }
        });
      });
    }
      //looping through all collections in the database
      // const mainKey = `${
      //   process.env.DATABASE_NAME || 'nest'
      // } - ${new Date().toUTCString()}`;

      let promisesArray = [];
      collectionNames.forEach(async (item, i) => {
          // key consists of database name and timestamp at the time of uploading
          // const key = `${mainKey}/${item}.json`;
          const tenantName = item.split('__')[0];
          const microservice = item.split('__')[1];
          const object = item.split('__')[2];
          const key = `${tenantName}/${microservice}/${object}.json`;

        /* 
          find() query returns a cursor(pointer) which points to the documents
          of the collection
        */
          const mongodbCursor = await this.mongoClient.db('timp').collection(item).find();

          const cursorArray = await mongodbCursor.toArray();

          const promise = uploadFile(bucketName, key, JSON.stringify(cursorArray));
          promisesArray.push(promise);
      });

      return Promise.all(promisesArray)
      .then(() => {
        // return mainKey;
        return 'success';
      })
      .catch((error) => {
        return `Error! Could not complete the database backup process. ${error}`;
      });

  }
}
