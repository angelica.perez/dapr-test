import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database/database.module';
import { ClientsModule, Transport } from '@nestjs/microservices';

const KAFKA_BROKER = process.env.KAFKA_BROKER || 'localhost:9094';

@Module({
  imports: [DatabaseModule, 
    ClientsModule.register([
      {
        name: 'UPDATE_SQL_SERVICE',
        transport: Transport.KAFKA,
        options: {
          client: {
            clientId: 'update-sql',
            brokers: [KAFKA_BROKER],
          },
          consumer: {
            groupId: 'update-sql-consumer',
          },
        },
      },
      {
        name: 'UPDATE_VECTOR_SERVICE',
        transport: Transport.KAFKA,
        options: {
          client: {
            clientId: 'update-vector',
            brokers: [KAFKA_BROKER],
          },
          consumer: {
            groupId: 'update-vector-consumer',
          },
        },
      },
    ])],
  controllers: [AppController],
  providers: [AppService, 
      {
      provide: 'KAFKA_PRODUCER',
      useFactory: async (kafkaClient) => {
        return kafkaClient.connect();
      },
      inject: ['UPDATE_SQL_SERVICE'],
    },
    {
      provide: 'KAFKA_PRODUCER',
      useFactory: async (kafkaClient) => {
        return kafkaClient.connect();
      },
      inject: ['UPDATE_VECTOR_SERVICE'],
    }
  ],
})
export class AppModule {}
