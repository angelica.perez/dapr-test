import { Module, Logger } from '@nestjs/common';
import { Global } from '@nestjs/common/decorators';
import { MongoClient } from 'mongodb';

@Global()
@Module({
  providers: [
    {
      provide: 'DATABASE_CONNECTION',
      useFactory: async () =>
        new Promise((resolve, reject) => {
          Logger.log('Connecting to Database');
          const MONGO_URL = process.env.MONGO_URL || 'mongodb://localhost:27017';

          MongoClient.connect(MONGO_URL, {})
           .then((client) => {
                Logger.log('Database Connection Successful');
                // let database = client.db(process.env.DATABASE_NAME || 'nest');
                resolve(client);
           })
           .catch((err) => {
                Logger.log('Database Connection Failed');
                reject(err);
              });
        }),
    },
  ],
  exports: ['DATABASE_CONNECTION'],
})
export class DatabaseModule {}