import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MinioService } from './minio/minio.service';
import { PostgresService } from './postgres/postgres.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, MinioService, PostgresService],
})
export class AppModule {}
