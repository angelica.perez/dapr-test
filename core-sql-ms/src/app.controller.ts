import { Controller, ValidationPipe } from '@nestjs/common';
import { AppService } from './app.service';
import { MessagePattern, Payload } from '@nestjs/microservices';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @MessagePattern('update_sql')
  updateSQL() {
    console.log('Update sql event received');
    return this.appService.updateSQL();
  }
}
