import { Injectable } from '@nestjs/common';
import * as Minio from 'minio';

@Injectable()
export class MinioService {
    private minioClient: Minio.Client;

  constructor() {
    this.minioClient = new Minio.Client({
      endPoint: process.env.S3_HOST || 'localhost',
      port: Number(process.env.S3_PORT) || 9000,
      useSSL: false,
      accessKey: process.env.S3_USER || 'minioadmin',
      secretKey: process.env.S3_PASSWORD || 'minioadmin'
    });
  }

  async readLatestFolder() {
    const objectList = await this.listObjects();
    //@ts-ignore
    return Promise.all(objectList.map(obj => this.readObject(bucketName, obj.name)));
    // return this.readObjectsFromFolder(bucketName, latestFolder);
  }

  async listObjects() {
    return new Promise((resolve, reject) => {
      const objects = [];
      const stream = this.minioClient.listObjectsV2('test-timp', '', true);
  
      stream.on('data', obj => objects.push(obj));
      stream.on('error', err => reject(err));
      stream.on('end', () => resolve(objects));
    });
  }

  async readObject(bucketName, objectName) {
    return new Promise((resolve, reject) => {
      let data = '';
      this.minioClient.getObject(bucketName, objectName, function(err, stream){
          stream.on('data', chunk => data += chunk);
          stream.on('end', () => resolve(data));
          stream.on('error', err => reject(err));

      });

    });
  }
}
