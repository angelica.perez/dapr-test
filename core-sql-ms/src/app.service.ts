import { Injectable } from '@nestjs/common';
import { MinioService } from './minio/minio.service';
import { PostgresService } from './postgres/postgres.service';

@Injectable()
export class AppService {
  constructor(private readonly minioService: MinioService, private readonly postgresService: PostgresService,) {}
  async updateSQL() {
    console.log('Update sql event received');
    const tableList = await this.minioService.listObjects();
    console.log(tableList);
    //@ts-ignore
    const tableNames = tableList.map(table => table.name.split('/')[1].split('.')[0].toUpperCase());
    console.log(tableNames);
    const jsonFiles = await this.minioService.readLatestFolder();
    console.log(jsonFiles);
    
    tableNames.forEach(async (tableName, index) => {
      const data = JSON.parse(jsonFiles[index]);
      data.forEach(async(item, i) => {
        await this.postgresService.createOrUpdateTable(tableName, item);
      })

      
    });
  }
}
