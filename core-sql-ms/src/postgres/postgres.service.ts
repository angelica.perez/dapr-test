import { Injectable } from '@nestjs/common';
import { Pool, Client } from 'pg';

@Injectable()
export class PostgresService {
    private pool: Pool;

  constructor() {}

  async createOrUpdateTable(tableName: string, jsonData: any) {
    console.log('createOrUpdateTable', jsonData);
    const client = new Client({
        host: process.env.POSTGRESQL_HOST || 'localhost',
        user: process.env.POSTGRESQL_USER || 'citizix_user',
        password: process.env.POSTGRESQL_PASSWORD || 'S3cret',
        port: Number(process.env.POSTGRESQL_PORT) || 5432,
    });
  
    await client.connect();
    this.pool = new Pool({
      user: process.env.POSTGRESQL_USER || 'citizix_user',
      host: process.env.POSTGRESQL_HOST || 'localhost',
      database: 'TIMP',
      password: process.env.POSTGRESQL_PASSWORD || 'S3cret',
      port: Number(process.env.POSTGRESQL_PORT) || 5432,
    });
    if (!await this.checkIfTableExists(tableName)) {
      await this.createTable(tableName, jsonData);
    }
    await this.upsertData(tableName, jsonData);
  }

  private async checkIfTableExists(tableName: string): Promise<boolean> {
    const query = `SELECT to_regclass('${tableName}');`;
    const res = await this.pool.query(query);
    return res.rows[0].to_regclass !== null;
  }

  private async createTable(tableName: string, jsonData: any) {
    const columns = Object.keys(jsonData).map(key => {
      if(key.toUpperCase() == '_ID'){
        return `${key.toUpperCase()} text PRIMARY KEY`;
      }
      return `${key.toUpperCase()} text`
    }).join(', ');
    console.log('columns', jsonData);
    const createQuery = `CREATE TABLE ${tableName} (${columns});`;
    console.log(createQuery);
    await this.pool.query(createQuery);
  }

  private async upsertData(tableName: string, jsonData: any) {
    const columns = Object.keys(jsonData).map(key => key.toUpperCase()).join(', ');
    const values = Object.values(jsonData).map(val => `'${val}'`).join(', ');
    const updateSet = Object.keys(jsonData).map(key => `${key.toUpperCase()} = EXCLUDED.${key.toUpperCase()}`).join(', ');

    console.log('columns', columns);
    console.log('values', values);
    console.log('updateSet', updateSet);

    // Assuming 'id' is the unique identifier column
    const upsertQuery = `
      INSERT INTO ${tableName} (${columns}) VALUES (${values})
      ON CONFLICT (_ID) DO UPDATE SET ${updateSet};
    `;

    console.log(upsertQuery);
    await this.pool.query(upsertQuery);
  }

}
